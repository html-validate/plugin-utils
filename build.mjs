/* eslint-disable no-console -- CLI script, expected to log */

import fs from "node:fs/promises";
import path from "node:path";
import { fileURLToPath } from "node:url";
import isCI from "is-ci";
import { build, analyzeMetafile } from "esbuild";
import { Extractor, ExtractorConfig } from "@microsoft/api-extractor";

const { externalDependencies } = JSON.parse(await fs.readFile("package.json", "utf-8"));

const entrypoint = {
	cjs: "src/entry-cjs.ts",
	esm: "src/entry-esm.ts",
};

const extension = {
	cjs: { js: ".cjs", ts: ".cts" },
	esm: { js: ".mjs", ts: ".mts" },
};

/**
 * @returns {Promise<void>}
 */
async function esbuild() {
	for (const format of ["cjs", "esm"]) {
		const result = await build({
			entryPoints: [{ in: entrypoint[format], out: "index" }],
			format,
			outdir: `dist/${format}`,
			bundle: true,
			platform: "node",
			target: "node18",
			sourcemap: true,
			metafile: true,
			logLevel: "info",
			outExtension: {
				".js": extension[format].js,
			},
			external: externalDependencies,
		});
		if (format === "esm") {
			console.log(await analyzeMetafile(result.metafile));
		}
	}
}

/**
 * @returns {Promise<void>}
 */
async function apiExtractor() {
	if (isCI) {
		console.log("Running API Extractor in CI mode.");
	} else {
		console.log("Running API Extractor in local mode.");
	}

	const filename = fileURLToPath(new URL("api-extractor.json", import.meta.url));
	const config = ExtractorConfig.loadFileAndPrepare(filename);

	for (const format of ["cjs", "esm"]) {
		config.mainEntryPointFilePath = path.resolve(`temp/types/entry-${format}.d.ts`);
		config.tsconfigFilePath = path.resolve(`tsconfig.${format}.json`);
		config.publicTrimmedFilePath = path.resolve(`dist/${format}/index.d${extension[format].ts}`);
		config.apiReportEnabled = format === "esm";
		const result = Extractor.invoke(config, {
			localBuild: !isCI,
			showVerboseMessages: true,
		});

		const { succeeded, errorCount, warningCount } = result;
		if (succeeded) {
			console.log(`API Extractor completed successfully`);
		} else {
			console.error(
				`API Extractor completed with ${errorCount} errors and ${warningCount} warnings`,
			);
			process.exitCode = 1;
		}
	}
}

/**
 * @returns {Promise<void>}
 */
async function run() {
	await fs.rm("dist", { recursive: true, force: true });
	await esbuild();
	await apiExtractor();
}

run().catch((err) => {
	console.error(err);
	process.exitCode = 1;
});
