# @html-validate/plugin-utils changelog

## 2.0.3 (2024-12-23)

### Bug Fixes

- **deps:** update html-validate to v9.0.0 ([b508c78](https://gitlab.com/html-validate/plugin-utils/commit/b508c78e6c0a0171ae2c19b8272d6877e650caf7))
- more explicit peerDependencies ([d726d06](https://gitlab.com/html-validate/plugin-utils/commit/d726d06add5d9d9ab37b2e1c372b48b9d34a630e))

## 2.0.2 (2024-12-21)

### Bug Fixes

- fix broken cjs build ([fbe6698](https://gitlab.com/html-validate/plugin-utils/commit/fbe6698c54f27869c9aca0a0ee246d8f5ce2ad7a))

## 2.0.1 (2024-12-21)

### Bug Fixes

- fix broken esm build ([98170b2](https://gitlab.com/html-validate/plugin-utils/commit/98170b296067774c82ae948405a5320015c41e63))

## 2.0.0 (2024-12-15)

### ⚠ BREAKING CHANGES

- require nodejs v18 or later
- require html-validate v8 or later

### Features

- **deps:** support espree v10 ([4e0bcb9](https://gitlab.com/html-validate/plugin-utils/commit/4e0bcb9fa507e66cbddc93d7aaf3e1426233a543))
- require html-validate v8 or later ([651a82e](https://gitlab.com/html-validate/plugin-utils/commit/651a82efd613dd52d4382f9f8432a89bd339a874))
- require nodejs v18 or later ([883afb6](https://gitlab.com/html-validate/plugin-utils/commit/883afb69294a29831b12c0c3b98dc4aed6b074e8))
- support html-validate v9 ([bca38c5](https://gitlab.com/html-validate/plugin-utils/commit/bca38c5a5358ce9589ded2ff73d2956ff118fecf))

## [1.0.2](https://gitlab.com/html-validate/plugin-utils/compare/v1.0.1...v1.0.2) (2023-06-04)

### Dependency upgrades

- **deps:** support html-validate v8 ([59ef6fd](https://gitlab.com/html-validate/plugin-utils/commit/59ef6fd300e49937ca4bc414035c05ec4e0e17d3))

## [1.0.1](https://gitlab.com/html-validate/plugin-utils/compare/v1.0.0...v1.0.1) (2023-06-04)

### Bug Fixes

- set types field in `package.json` ([b11c5c2](https://gitlab.com/html-validate/plugin-utils/commit/b11c5c29a506a05a87e2a05ac9ca31e8712da6a3))

## 1.0.0 (2023-05-21)

### Features

- initial release ([18e15ec](https://gitlab.com/html-validate/plugin-utils/commit/18e15ec25e187b34f3ee78089e26af8aef127f36))
