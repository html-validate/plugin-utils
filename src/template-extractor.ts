/* eslint-disable @typescript-eslint/no-non-null-assertion -- declarations say
 * location fields are optional but they are always present when `{loc: true}` */

import * as espree from "espree";
import * as walk from "acorn-walk";

/* eslint-disable-next-line n/no-extraneous-import -- type is pulled via acorn-walk */
import { type Node } from "acorn";

import { type Source } from "html-validate";
import { positionToOffset } from "./position-to-offset";

/* espree puts location information a bit different than estree */
declare module "estree" {
	interface TemplateElement {
		start: number;
		end: number;
	}
}

function joinTemplateLiteral(nodes: espree.TemplateElement[]): string {
	let offset = nodes[0].start + 1;
	let output = "";
	for (const node of nodes) {
		output += " ".repeat(node.start + 1 - offset);
		output += node.value.raw;
		offset = node.end - 2;
	}
	return output;
}

function extractLiteral(
	node: espree.Expression | espree.Pattern | espree.Literal | espree.BlockStatement,
	filename: string,
	data: string,
): Source | null {
	switch (node.type) {
		/* ignored nodes */
		case "FunctionExpression":
		case "Identifier":
			return null;

		case "Literal":
			if (typeof node.value !== "string") {
				return null;
			}
			return {
				data: node.value.toString(),
				filename,
				line: node.loc!.start.line,
				column: node.loc!.start.column + 1,
				offset: positionToOffset(node.loc!.start, data) + 1,
			};

		case "TemplateLiteral":
			return {
				data: joinTemplateLiteral(node.quasis),
				filename,
				line: node.loc!.start.line,
				column: node.loc!.start.column + 1,
				offset: positionToOffset(node.loc!.start, data) + 1,
			};

		case "TaggedTemplateExpression":
			return {
				data: joinTemplateLiteral(node.quasi.quasis),
				filename,
				line: node.quasi.loc!.start.line,
				column: node.quasi.loc!.start.column + 1,
				offset: positionToOffset(node.quasi.loc!.start, data) + 1,
			};

		case "ArrowFunctionExpression": {
			const whitelist = ["Literal", "TemplateLiteral"];
			if (whitelist.includes(node.body.type)) {
				return extractLiteral(node.body, filename, data);
			} else {
				return null;
			}
		}

		/* istanbul ignore next: this only provides a better error, all currently known nodes are tested */
		default: {
			const loc = node.loc!.start;
			const line = String(loc.line);
			const column = String(loc.column);
			const context = `${filename}:${line}:${column}`;
			throw new Error(`Unhandled node type "${node.type}" at "${context}" in extractLiteral`);
		}
	}
}

function compareKey(
	node: espree.Expression | espree.PrivateIdentifier,
	key: string,
	filename: string,
): boolean {
	switch (node.type) {
		case "Identifier":
			return node.name === key;

		case "Literal":
			return node.value === key;

		/* istanbul ignore next: this only provides a better error, all currently known nodes are tested */
		default: {
			const loc = node.loc!.start;
			const line = String(loc.line);
			const column = String(loc.column);
			const context = `${filename}:${line}:${column}`;
			throw new Error(`Unhandled node type "${node.type}" at "${context}" in compareKey`);
		}
	}
}

/**
 * @public
 * @since 1.0.0
 */
export class TemplateExtractor {
	private ast: espree.Program;

	private filename: string;
	private data: string;

	private constructor(ast: espree.Program, filename: string, data: string) {
		this.ast = ast;
		this.filename = filename;
		this.data = data;
	}

	/**
	 * Create a new [[TemplateExtractor]] from javascript source code.
	 *
	 * `Source` offsets will be relative to the string, i.e. offset 0 is the first
	 * character of the string. If the string is only a subset of a larger string
	 * the offsets must be adjusted manually.
	 *
	 * @param source - Source code.
	 * @param filename - Optional filename to set in the resulting
	 * `Source`. Defauls to `"inline"`.
	 */
	public static fromString(source: string, filename?: string): TemplateExtractor {
		const ast = espree.parse(source, {
			ecmaVersion: "latest",
			sourceType: "module",
			loc: true,
		});
		return new TemplateExtractor(ast, filename ?? "inline", source);
	}

	/**
	 * Extract object properties.
	 *
	 * Given a key `"template"` this method finds all objects literals with a
	 * `"template"` property and creates a [[Source]] instance with proper offsets
	 * with the value of the property. For instance:
	 *
	 * ```
	 * const myObj = {
	 *   foo: 'bar',
	 * };
	 * ```
	 *
	 * The above snippet would yield a `Source` with the content `bar`.
	 *
	 */
	public extractObjectProperty(key: string): Source[] {
		const result: Source[] = [];
		const { filename, data } = this;
		const node: Node = this.ast as unknown as Node;
		walk.simple(node, {
			Property(node: espree.Property) {
				if (compareKey(node.key, key, filename)) {
					const source = extractLiteral(node.value, filename, data);
					if (source) {
						source.filename = filename;
						result.push(source);
					}
				}
			},
		} as unknown as walk.SimpleVisitors<unknown>);
		return result;
	}
}
