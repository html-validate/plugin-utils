/**
 * Given an offset into a source, calculate the corresponding line and column.
 *
 * @public
 * @since 1.0.0
 */
export function positionFromOffset(text: string, offset: number): [line: number, column: number] {
	let line = 1;
	let prev = 0;
	let pos = text.indexOf("\n");

	/* step one line at a time until newline position is beyond wanted offset */
	while (pos !== -1) {
		if (pos >= offset) {
			return [line, offset - prev + 1];
		}
		line++;
		prev = pos + 1;
		pos = text.indexOf("\n", pos + 1);
	}

	/* missing final newline */
	return [line, offset - prev + 1];
}
