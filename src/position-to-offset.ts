import { type Position } from "./position";

/**
 * Compute source offset from line and column and the given markup.
 *
 * @public
 * @since 1.0.0
 * @param position - Line and column.
 * @param data - Source markup.
 * @returns The byte offset into the markup which line and column corresponds to.
 */
export function positionToOffset(position: Position, data: string): number {
	let line = position.line;
	let column = position.column + 1;
	for (let i = 0; i < data.length; i++) {
		if (line > 1) {
			/* not yet on the correct line */
			if (data[i] === "\n") {
				line--;
			}
		} else if (column > 1) {
			/* not yet on the correct column */
			column--;
		} else {
			/* line/column found, return current position */
			return i;
		}
	}
	/* istanbul ignore next: should never reach this line unless espree passes bad
	 * positions, no sane way to test */
	throw new Error("Failed to compute location offset from position");
}
