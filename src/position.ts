/**
 * Represents line and column.
 *
 * @public
 * @since 1.0.0
 */
export interface Position {
	/** Line number (First line is 1) */
	line: number;

	/** Column number (first column is 1) */
	column: number;
}
